/*
Copyright (C) 2017-2020 The Kira Developers (see AUTHORS file)

This file is part of Kira.

Kira is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Kira is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Kira.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <string>

class ExceptionCommandLine : public std::exception {
  std::string msg;
public:
  ExceptionCommandLine(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionCommandLine: " + msg).c_str();
  }
};

class ExceptionConfigFiles : public std::exception {
  std::string msg;
public:
  ExceptionConfigFiles(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionConfigFiles: " + msg).c_str();
  }
};

class ExceptionDisk : public std::exception {
  std::string msg;
public:
  ExceptionDisk(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionDisk: " + msg).c_str();
  }
};

class ExceptionFermat : public std::exception {
  std::string msg;
public:
  ExceptionFermat(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionFermat: " + msg).c_str();
  }
};

class ExceptionInternal : public std::exception {
  std::string msg;
public:
  ExceptionInternal(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionInternal: " + msg).c_str();
  }
};

class ExceptionPipe : public std::exception {
  std::string msg;
public:
  ExceptionPipe(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionPipe: " + msg).c_str();
  }
};

class ExceptionResume : public std::exception {
  std::string msg;
public:
  ExceptionResume(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionResume: " + msg).c_str();
  }
};

class ExceptionRuntime : public std::exception {
  std::string msg;
public:
  ExceptionRuntime(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionRuntime: " + msg).c_str();
  }
};

// not a real exception, just quit the program and return 0
class ExceptionQuit : public std::exception {
  std::string msg;
public:
  ExceptionQuit(const std::string& msg_ = "Unspecified error") : msg(msg_) {};
  const char* what() const throw () {
    return ("Kira::ExceptionQuit: " + msg).c_str();
  }
};

#endif
